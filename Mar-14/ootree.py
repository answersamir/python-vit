class Tree:
    def __init__(self,d,l,r):
        self.data  = d
        self.left  = l
        self.right = r
    def __repr__(self):
        return  ("Br " + str(self.data) + "(" + repr(self.left) + "," +
            repr(self.right) +")"
         )

t0 = Tree(2,None, None)
t1 = Tree(3,t0, t0)
