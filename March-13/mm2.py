from enum import Enum

class Trtag(Enum):
    br=1
    et=2
    def __repr__(self):
        return self.name
   
et = Trtag.et
br = Trtag.br

def dfs(T):
    if(T == et):
        return
    else:
        (br,value,left,right)=T
        dfs(left)
        print(value)
        dfs(right)
