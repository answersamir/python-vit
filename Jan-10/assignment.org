#+TITLE: Assignment 1
#+OPTIONS: toc:nil
* Paradigm
Read up on Kuhn Paradigm shift and tell about its application to CS
* Read languager blog
In particular

http://blog.languager.org/2012/10/functional-programming-lost-booty.html
http://blog.languager.org/2011/02/cs-education-is-fat-and-weak-1.html 
http://www.the-magus.in/Publications/chor.pdf
and sequel

If you understand it all read
http://blog.languager.org/2013/08/applying-si-on-sicp.html

If you understand that all you probably should not register for this 
course.

How much of the above is history/philosophy/science?

Or
* Technology
** Setups
- python
- python3
- idle
- idle3
- python-doc
- python3-doc
** Help
- Idle help
- Python help — interactive
- Python help — [[file:///usr/share/doc/python3.4/html/index.html][offline docs 3.4]]
- Python help — [[file:///usr/share/doc/python2.7/html/index.html][offline docs 2.7]]
- Python help — online docs
** Contact
- emails
- bitbucket
